FROM node:8

WORKDIR /usr/src/app/backend
COPY package*.json ./

RUN npm install

COPY . .
